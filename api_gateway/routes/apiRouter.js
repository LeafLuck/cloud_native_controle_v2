// apiRouter.js
import { Router } from 'express';
import { authenticateToken } from '../middleware/auth.js';
var httpProxy = require('http-proxy');
const apiRouter = Router();
const proxy = httpProxy.createProxyServer({});


// Route pour les équipes
apiRouter.get('/equipes', authenticateToken, (req, res) => {
    proxy.web(req, res, { target: 'http://equipe_joueur_service:8000/equipes' }, (error) => {
      console.error(error);
      res.status(500).json({
        message: 'Une erreur est survenue lors de la récupération des équipes',
        error: error.message
      });
    });
  });

apiRouter.get('/equipes/:id', authenticateToken, (req, res) => {
    proxy.web(req, res, { target: 'http://equipe_joueur_service:8000/equipes' }, (error) => {
      console.error(error);
      res.status(500).json({
        message: 'Une erreur est survenue lors de la récupération de l équipe',
        error: error.message
      });
    });
  });

apiRouter.post('/equipes', authenticateToken, (req, res) => {
    proxy.web(req, res, { target: 'http://equipe_joueur_service:8000/equipe' , method: 'POST' }, (error) => {
      console.error(error);
      res.status(500).json({
        message: 'Une erreur est survenue lors de la creation de l équipes',
        error: error.message
      });
    });
 });
  

 apiRouter.put('/equipes/:id', authenticateToken, (req, res) => {
  proxy.web(req, res, { target: `http://equipe_joueur_service:8000/equipes/${req.params.id}`, method: 'PUT' }, (error) => {
    console.error(error);
    res.status(500).json({
      message: 'Une erreur est survenue lors de la modification de l équipe',
      error: error.message
    });
  });
});

apiRouter.delete('/equipes/:id', authenticateToken, (req, res) => {
  proxy.web(req, res, { target: `http://equipe_joueur_service:8000/equipes/${req.params.id}`, method: 'DELETE' }, (error) => {
    console.error(error);
    res.status(500).json({
      message: 'Une erreur est survenue lors de la deletion de l équipe',
      error: error.message
    });
  });
});
  // Proxy pour les joueurs
  apiRouter.get('/joueurs', authenticateToken, (req, res) => {
    proxy.web(req, res, { target: 'http://equipe_joueur_service:8000/joueurs' }, (error) => {
      console.error(error);
      res.status(500).json({
        message: 'Une erreur est survenue lors de la récupération des joueurs',
        error: error.message
      });
    });
  });
  apiRouter.get('/joueurs/:id', authenticateToken, (req, res) => {
    proxy.web(req, res, { target: 'http://equipe_joueur_service:8000/joueurs' }, (error) => {
      console.error(error);
      res.status(500).json({
        message: 'Une erreur est survenue lors de la récupération de l équipe',
        error: error.message
      });
    });
  });

apiRouter.post('/joueurs', authenticateToken, (req, res) => {
    proxy.web(req, res, { target: 'http://equipe_joueur_service:8000/joueurs' , method: 'POST' }, (error) => {
      console.error(error);
      res.status(500).json({
        message: 'Une erreur est survenue lors de la creation du joueurs',
        error: error.message
      });
    });
 });
  

 apiRouter.put('/joueurs/:id', authenticateToken, (req, res) => {
  proxy.web(req, res, { target: `http://equipe_joueur_service:8000/joueurs/${req.params.id}`, method: 'PUT' }, (error) => {
    console.error(error);
    res.status(500).json({
      message: 'Une erreur est survenue lors de la modification du joueurs',
      error: error.message
    });
  });
});

apiRouter.delete('/joueurs/:id', authenticateToken, (req, res) => {
  proxy.web(req, res, { target: `http://equipe_joueur_service:8000/equipes/${req.params.id}`, method: 'DELETE' }, (error) => {
    console.error(error);
    res.status(500).json({
      message: 'Une erreur est survenue lors de la deletion du joueur',
      error: error.message
    });
  });
});
export default apiRouter;
